"user strict";




let image = document.querySelectorAll(".image-to-show");
let firstPicture = image[0];
let lastPicture = image[image.length - 1];
let stopButton = document.querySelector("#stop");
let resumeButton = document.querySelector("#resume");


let slider = () =>{
  let mainPicture = document.querySelector(".show");
  console.log(mainPicture);
if(mainPicture !== lastPicture){
  mainPicture.classList.remove("show");
  mainPicture.nextElementSibling.classList.add("show");
}else{
  mainPicture.classList.remove("show");
  firstPicture.classList.add("show");
}
}


let timer = setInterval(slider, 3000);
stopButton.addEventListener("click", () =>{
  clearInterval(timer);
  resumeButton.disabled = false;
  stopButton.disabled = true;
} );

resumeButton.addEventListener("click", () =>{
timer = setInterval(slider, 3000);
resumeButton.disabled = true;
stopButton.disabled = false;
});
